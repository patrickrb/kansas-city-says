$(document).ready(function() {
    markersArray = [];
    currentMarkerSize = 80;

    $('#thumbnailSizeSlider').slider().on('slide', function(ev){
        currentMarkerSize = ev.value;
        setTweetTextWidth(ev.value);
        $('#map-canvas .custom_marker img').width(ev.value + 'px').height(ev.value + 'px');
        $('.custom_marker').width(ev.value + 'px').height(ev.value + 'px');
    });

    $('#deleteMarkers').click(function(){
        deleteAllMarkers();
    });

    $('#togglePause').click(function(){
        if(updatesPaused == true){
            updatesPaused = false;
            $('#togglePause').html('Pause Updates');
        }
        else if(updatesPaused == false){
            updatesPaused = true;
            $('#togglePause').html('Resume Updates');
        }
    });

    function initialize() {
          updatesPaused = false;
          var userLong = "-94.7425";
          var userLat = "38.9492";
          markerIndex = 0;
          var mapOptions = {
            center: new google.maps.LatLng(userLat, userLong),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          map = new google.maps.Map(document.getElementById("map-canvas"),
              mapOptions);
        }
        google.maps.event.addDomListener(window, 'load', initialize);

        socket = io.connect('http://burnsforcedevelopment.com:8080');
        socket.on('startup',function(){
            $.jGrowl('<i class="icon-ok-sign" style="color:green;font-size:22px;"></i>Connected to websocket server.', { position: 'bottom-right' });
        });

        socket.on('newTweet', function(data){
            if(updatesPaused == false){
                var openWidth = currentMarkerSize + 200;
                var lat = data.geo.coordinates[0];
                var long = data.geo.coordinates[1];
                var myLatlng = new google.maps.LatLng(lat,long);
                var marker = new RichMarker({
                    position: myLatlng,
                    flat: true,
                    map: map,
                    zIndex: google.maps.Marker.MAX_ZINDEX + 1,
                    content: '<div class="custom_marker" id="marker-' + markerIndex + '" style="width:' + openWidth + 'px; height:'+ currentMarkerSize + 'px;"><div class="marker-left"><img src="' + data.user.profile_image_url + '" onclick="clickTweet(this)" width="' + currentMarkerSize + 'px" height="' + currentMarkerSize + 'px"/></div><div class="marker-right"><div class="marker-right-header"><div class="marker-username"><a href="http://twitter.com/'+ data.user.screen_name + '" target="_blank">@' + data.user.screen_name + '</a></div></div><p>' + processTweetLinks(data.text) + '</p></div></div>'
                });
                var numMarkers = markersArray.length;
                if(numMarkers >= 50){
                    deleteMarker(markersArray[0]);
                    markersArray.splice(0, 1);
                }
                markersArray.push(marker);
                google.maps.event.addListener(marker, 'click', function() {
                    $.each( markersArray, function(i, marker ){
                        marker.setZIndex(google.maps.Marker.MAX_ZINDEX);
                    });
                    marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
                });
                closeAllMarkers();
                marker.setMap(map);
                markerIndex++;
            }
        });
});
function getAllMarkers(){
    var returnArray = [];
    $('.custom_marker').each(function(){
        returnArray.push($(this));
    });
    return returnArray;
}

function setTweetTextWidth(width){
    if(width == "50")
        $('.custom_marker span').width('78%');
    else if(width == "100")            
        $('.custom_marker span').width('65%');
}

function deleteMarker(marker) {
    marker.setMap(null);
}

function deleteAllMarkers(){
    $.each( markersArray, function(i, marker ){
        marker.setMap(null);
    });
    markersArray = [];
}

function htmlEncode(value){
    return $('<div/>').text(value).html();
}

function htmlDecode(value){
    return $('<div/>').html(value).text();
}

function toggleMarkerWidth(clickedTweet){
    var clickedTweetSpan = $(clickedTweet).find( "span" );
    var currentWidth =$(clickedTweet).width();
    var newWidth = currentWidth + 200;
    if(currentWidth == currentMarkerSize){
        clickedTweet.animate({
            width: newWidth + "px"
        }, 300, function(){
            $(clickedTweetSpan).fadeIn('slow');
        });
    }
    else{
        $(clickedTweetSpan).fadeOut('fast',function(){
           clickedTweet.animate({
                width: currentMarkerSize + "px"
           }, 300); 
        });
    }
}

function closeAllMarkers(){
    $('.custom_marker').each(function(){
        $(this).find('span').hide();   
        $(this).animate({
            width: currentMarkerSize + "px"
       }, 300); 
    });
}

function clickTweet(event){
    closeAllMarkers();
    var clickedTweet = $(event).parent().parent();
    console.log(clickedTweet);
    toggleMarkerWidth(clickedTweet);

}

function processTweetLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
    text = text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
    exp = /(^|\s)#(\w+)/g;
    text = text.replace(exp, "$1<a href='https://twitter.com/search?q=%23$2' target='_blank'>#$2</a>");
    exp = /(^|\s)@(\w+)/g;
    text = text.replace(exp, "$1<a href='http://www.twitter.com/$2' target='_blank'>@$2</a>");
    return text;
}