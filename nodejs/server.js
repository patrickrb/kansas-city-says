var twitter = require('ntwitter');
var io = require('socket.io').listen(8080,{
  'log level':1
});

io.sockets.on('connection', function(socket){
    socket.emit('startup');
});

var twit = new twitter({
  consumer_key: '',
  consumer_secret: '',
  access_token_key: '',
  access_token_secret: ''
});

twit.stream('statuses/filter', {'locations':'-95,38.8,-94.32,39.40'}, function(stream) {
  stream.on('data', function (data) {
    var tweetText = data.text;
    console.log(tweetText);
    io.sockets.emit('newTweet', data);
  });
});